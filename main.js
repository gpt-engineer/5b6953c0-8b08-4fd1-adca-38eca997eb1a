document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();

    // Get the task input value
    var taskInput = document.getElementById('task-input');
    var taskValue = taskInput.value.trim();

    // Check if the input value is not empty
    if (taskValue) {
        // Create a new task item
        var taskItem = document.createElement('li');
        taskItem.textContent = taskValue;
        taskItem.classList.add('bg-white', 'p-4', 'rounded', 'flex', 'justify-between', 'items-center');

        // Create a delete button
        var deleteButton = document.createElement('button');
        deleteButton.innerHTML = '<i class="fas fa-trash-alt"></i>';
        deleteButton.classList.add('text-red-500');
        deleteButton.addEventListener('click', function() {
            taskItem.remove();
        });

        // Append the delete button to the task item
        taskItem.appendChild(deleteButton);

        // Append the task item to the task list
        document.getElementById('task-list').appendChild(taskItem);

        // Clear the task input value
        taskInput.value = '';
    }
});
